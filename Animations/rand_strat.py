#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from manimlib.imports import *

THK_RED    = "#c81e0f"
THK_ORANGE = "#ea5a00"
THK_VIOLET = "#b43092"

BIT_COLOR_MAP = {0: THK_ORANGE, 1: THK_VIOLET }

class Student(VMobject):
    """ student represented by matriculation number """
    def __init__(self, i, **kwargs):
        VMobject.__init__(self, **kwargs)
        self.index = i
        self.matno = str(1101 + i)
        # self.attribute = np.random.randint(2) # 0 or 1
        self.attribute = i not in [0, 1, 3, 6]
        self.target = None
        self.submobjects.append(
            RoundedRectangle(width = 1.8, height=0.5,
                             color = self.color,
                             fill_opacity=0.9,
                             corner_radius=0.1))
        self.submobjects.append(
            TextMobject(self.matno, color=BLACK))

        self.to_edge(UP).shift(0.6*i*DOWN + 2.5*RIGHT)
            
    def set_color(self, color, family=True):
        self.submobjects[0].set_color(color, family=family)
        if self.target:
            self.target.set_color(color, family=family)

    def get_color(self):
        return self.color

    def attribute_color(self):
        return BIT_COLOR_MAP[ self.attribute ]

    def color_by_attribute(self):
        self.set_color(self.attribute_color())

    def color_by_map(self, i):
        self.set_color(BIT_COLOR_MAP[i])


class Test(Scene):
    def construct(self):
        s = Student(1)
        # show creation
        self.play(ShowCreation(s))
        self.wait()
        # color
        self.play(FadeToColor(s, s.attribute_color()))
        self.wait()
        # move to first target
        s.generate_target(use_deepcopy=False) # target is colored too
        s.target.to_edge(UP)
        self.play(MoveToTarget(s))
        self.wait()
        # move to next target
        s.target.shift(4*DOWN)
        self.play(MoveToTarget(s)) # target can be reused
        self.wait()


class Cohorte(Scene):
    """ Create a cohorte of some students and display them """
    def setup(self):
        self.size     = 12
        self.cohorte  = [Student(i) for i in range(self.size)]
        self.vcohorte = VGroup(*self.cohorte)
        self.position = [mo.get_center() for mo in self.cohorte]
        # self.clabel  = self.set_clabel("Studierende:") # doesn't work
        self.clabel   = TextMobject("Studierende:").to_corner(UP + LEFT)
        self.group1   = {'text': TextMobject("Gruppe 1"), 'col': 0.5*RIGHT}
        self.group2   = {'text': TextMobject("Gruppe 2"), 'col': 4.5*RIGHT}
        self.ilist    = None

    def set_clabel(self, *args):
        self.clabel = TextMobject(*args).to_corner(UP + LEFT)

    def set_ilist(self, name, content):
        content_str = ["("]
        for (i,_) in enumerate(content):
            content_str.append(str(content[i]))
            content_str.append(", ")
        content_str[-1]=")" # replace last comma by bracket
        name_mo = TextMobject(name).to_corner(UP + LEFT).shift(1.5*DOWN)
        list_mo = TexMobject(*content_str).scale(0.8).next_to(name_mo, DOWN).align_to(name_mo, LEFT)
        self.ilist = {'name': name_mo, 'numbers': list_mo}

    def write_ilist(self, pause=1):
        # self.play(FadeOut(self.clabel))
        self.play(Write(self.ilist['name']))
        self.play(Write(self.ilist['numbers']))
        self.wait(pause)

    def show_cohorte(self, pause=1):
        self.play(Write(self.clabel))
        self.play(ShowCreation(self.vcohorte))
        self.wait(pause)

    def color_by_attribute(self):
        self.play(*[FadeToColor(mo, mo.attribute_color()) for mo in self.cohorte])

    def write_group_names(self, yshift):
        for g in (self.group1, self.group2):
            self.play(Write(g['text'].move_to(g['col'] + yshift*DOWN)))
    
    def construct(self):
        self.show_cohorte()
        if self.ilist:
            self.write_ilist()
        self.wait(2)
        self.color_by_attribute()
        self.wait(2)
        

class Randomisation(Cohorte):
    def setup(self):
        Cohorte.setup(self)
        self.set_randomisation()
        groups = [ [], [] ]
        for (bit, mo) in zip(self.rand, self.cohorte):
            mo.generate_target()
            mo.target.shift(2*(RIGHT if bit else LEFT))
            groups[bit].append(mo)
        self.mover = []
        for g in groups:
            for (i, mo) in enumerate(g):
                mo.target.to_edge(UP).shift(i*0.6*DOWN)
                self.mover.append(MoveToTarget(mo))

    def set_randomisation(self):
        self.rand = np.random.randint(0, 2, self.size)
        self.set_ilist("Randomisierung:", self.rand)

    def construct(self):
        self.show_cohorte()
        # show randomisation
        self.write_ilist()
        self.play(*[FadeToColor(mo, BIT_COLOR_MAP[b]) 
                    for (mo, b) in zip(self.cohorte, self.rand)])
        self.wait(2)
        self.play(*self.mover)
        self.write_group_names(2.2)
        self.wait(4)

            
class Permutation(Cohorte):
    """ Permutate cohorte and distribute to groups """
    def setup(self):
        Cohorte.setup(self)
        self.set_permutation()
        self.apply_permutation(self.perm)

    def set_permutation(self):
        self.perm = np.random.permutation(self.size)
        self.set_ilist("Permutation:", [p + 1 for p in self.perm])

    def apply_permutation(self, permutation):
        # set targets for permutation
        inv_perm = np.empty_like(permutation)
        inv_perm[permutation] = np.arange(permutation.size)
        print(f"Permutation: {permutation}")
        print(f"inv. perm. : {inv_perm}")
        for i, mo in zip(inv_perm, self.cohorte):
            mo.generate_target()
            mo.target.move_to(self.position[i])

    def apply_grouping(self):
        for i in range(self.size):
            mo = self.cohorte[ self.perm[i] ]
            x = 0.5 + 4*(i%2)
            y = 3.2 - 0.3*(i - i%2)
            mo.generate_target()
            mo.target.move_to(x*RIGHT + y*UP)

    def apply_even_odd_coloring(self):
        color_changer = []
        for i in range(self.size):
            mo = self.cohorte[ self.perm[i] ]
            assert mo.target, "cohorte member has no target"
            mo = self.cohorte[ self.perm[i] ]
            even_odd_col = BIT_COLOR_MAP[i%2]
            mo.target.set_color(even_odd_col)
            color_changer.append(FadeToColor(mo, even_odd_col))
        self.play(*color_changer)
            
    def construct(self):
        # display cohorte
        self.show_cohorte()
        self.write_ilist()
        self.wait(2)
        # shuffle cohorte
        self.play(*[MoveToTarget(mo, path_arc=2.5) for mo in self.cohorte])
        self.wait(2)
        # indicate permutations
        for i in range(4):
            self.play(Indicate(self.ilist['numbers'][2*i+1], color=THK_RED), 
                      Indicate(self.cohorte[ self.perm[i] ], color=THK_RED))
        self.wait(1)
        # rearrange into groups
        self.apply_grouping()
        self.apply_even_odd_coloring()
        for i in range(3):
            j = self.perm[i]
            self.play(MoveToTarget(self.cohorte[j], path_arc=0.5))
        self.play(*[MoveToTarget(mo, path_arc=0.5) for mo in self.cohorte])
        self.write_group_names(1)
        self.wait(4)

class PermutationWithAttribute(Permutation):
    def setup(self):
        Permutation.setup(self)
        self.label_a = TextMobject("\\quad(\\kern-0.1em", "mit Merkmal", "\\kern-0.3em)", buff=SMALL_BUFF)
        self.label_a.next_to(self.clabel, RIGHT)
        self.label_a.submobjects[1].set_color_by_gradient(THK_ORANGE, THK_VIOLET)

    def construct(self):
        # display cohorte
        self.show_cohorte()
        self.wait()
        # display cohorte with attributes
        self.play(Write(self.label_a))
        self.play(*[FadeToColor(student, student.attribute_color())
                    for student in self.cohorte])
        # shuffle cohorte
        # self.remove(self.label_a)
        self.write_ilist()
        self.play(*[MoveToTarget(mo, path_arc=2.5) for mo in self.cohorte])
        self.wait()
        # rearrange into groups
        self.apply_grouping()
        self.play(*[MoveToTarget(mo, path_arc=0.5) for mo in self.cohorte])
        self.write_group_names(0.5)
        self.wait(4)


class Stratification(Permutation):
    def setup(self):
        Permutation.setup(self)
        self.label_a = TextMobject("\\quad(\\kern-0.1em", "mit Merkmal", "\\kern-0.3em)", buff=SMALL_BUFF)
        self.label_a
        self.label_a.next_to(self.clabel, RIGHT)
        self.label_a.submobjects[1].set_color_by_gradient(THK_ORANGE, THK_VIOLET)

    def apply_classify(self):
        """ rearrange by attribute """
        attrs = np.array([s.attribute for s in self.cohorte])
        attrs_p = np.empty_like(attrs)
        attrs_p[self.perm] = attrs
        c_vector = attrs_p.argsort()
        print(f"classifying: {c_vector}")
        fix_me = np.array([6, 1, 3, 0, 11, 4, 10, 2, 8, 7, 9, 5])
        print(f"fix_me:      {fix_me}")
        self.apply_permutation(fix_me)
        
    def apply_grouping(self):
        color_changer = []
        fix_me = np.array([6, 1, 3, 0, 11, 4, 10, 2, 8, 7, 9, 5])
        for i in range(self.size):
            mo = self.cohorte[ fix_me[i] ]
            x = 0.5 + 4*(i%2)
            y = 3.2 - 0.3*(i - i%2)
            mo.generate_target()
            mo.target.move_to(x*RIGHT + y*UP)

    def construct(self):
        # display cohorte
        self.show_cohorte()
        self.wait()
        # display cohorte with attributes
        self.play(Write(self.label_a))
        self.play(*[FadeToColor(student, student.attribute_color())
                    for student in self.cohorte])
        # shuffle cohorte
        # self.remove(self.label_a)
        self.write_ilist()
        self.play(*[MoveToTarget(mo, path_arc=2.5) for mo in self.cohorte])
        self.wait()
        # sort by attribute
        self.apply_classify()
        self.play(*[MoveToTarget(mo, path_arc=2.5) for mo in self.cohorte])
        self.wait(2)
        # rearrange into groups
        self.apply_grouping()
        self.play(*[MoveToTarget(mo, path_arc=0.5) for mo in self.cohorte])
        self.write_group_names(0.5)
        self.wait(4)
