#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from manimlib.imports import *

import numpy as np

THK_RED    = "#c81e0f"
THK_ORANGE = "#ea5a00"
THK_VIOLET = "#b43092"

BIT_COLOR_MAP = {0: THK_ORANGE, 1: THK_VIOLET }

WIDTH = 2*RIGHT
HEIGHT = 0.6*UP

class SpreadSheet(Scene):
    """ draw a grid like in a spreadsheet """
    def setup(self):
        self.cols = 2
        self.rows = 12
        self.column = [None for _ in range(2)]
        self.formula_field = 4*LEFT # where formulae are input

    def create_spreadsheet(self):
        table = []
        # draw grid
        for i in range(self.rows + 1):
            table.append(Line(ORIGIN, self.cols*WIDTH).shift(i*HEIGHT))
        for j in range(self.cols + 1):
            table.append(Line(ORIGIN, self.rows*HEIGHT).shift(j*WIDTH))
        # line numbers
        for i in range(self.rows):
            label = TextMobject(str(self.rows - i))
            label.move_to((i + 0.5)*HEIGHT - 0.2*WIDTH)
            table.append(label)
        # column lables
        for j in range(self.cols):
            label = TextMobject(chr(ord('A') + j))
            label.move_to((self.rows + 0.5)*HEIGHT + (j + 0.5)*WIDTH)
            table.append(label)
        # shift whole table down
        for element in table:
            element.shift(4*DOWN)
        self.play(ShowCreation(VGroup(*table)))

    def position(self, col, row):
        x = (col + 0.5)*WIDTH
        y = (self.rows - row - 0.5)*HEIGHT
        return x + y + 4*DOWN

    def create_column(self, col, formulas, values):
        mo_f = [TextMobject(f).move_to(self.formula_field)
                for f in formulas]
        mo_v = [TextMobject(val).scale(0.8).move_to(self.position(col, row))
                 for row, val in enumerate(values)]
        self.column[col] = {'formulas': mo_f, 'values': mo_v}

    def write_column(self, j):
        c = self.column[j]
        for i, f in enumerate(c['formulas']):
            self.play(Write(f))
            self.wait()
            self.play(ReplacementTransform(f, c['values'][i]))
        self.wait(2)
        other_cells = c['values'][(len(c['formulas'])):]
        self.play(*[Write(v) for v in other_cells])
                             
    def construct(self):
        self.create_spreadsheet()
        self.wait()


class SpreadSheetRandoms(SpreadSheet):
    """ simple spreadsheet with random numbers in column A """
    def setup(self):
        super().setup()
        self.randoms = np.random.random(self.rows)
        self.create_column(0, 
                           ["=RAND()", "=ZUFALL()"],
                           [f"{val:6.4f}" for val in self.randoms])

    def construct(self):
        super().construct()
        self.write_column(0)
        self.wait()

        
class SpreadSheetPermutation(SpreadSheetRandoms):
    """ create a random permuatation in the spreadsheet """
    def setup(self):
        super().setup()
        self.create_column(1,
                           [],
                           [str(i + 1) for i in range(self.rows)])

    def sort_matrix(self):
        sort_vector = np.argsort(self.randoms)
        inv_vector = np.empty_like(sort_vector)
        inv_vector[sort_vector] = np.arange(sort_vector.size)
        print(f"sort vector: {sort_vector}")
        print(f"inv. vector: {inv_vector}")
        moves = []
        for c in self.column:
            for i, mo in enumerate(c['values']):
                mo.generate_target()
                t_position = c['values'][inv_vector[i]]
                mo.target.move_to(t_position)
            moves.extend([MoveToTarget(mo) for mo in c['values']])
        self.play(Write(TextMobject("Sortieren nach A").move_to(4*LEFT + 2*DOWN)))
        self.play(*moves)

    def highlight_column(self, col):
        self.play(*[FadeToColor(mo, THK_ORANGE)
                    for mo in self.column[col]['values']])
            
    def construct(self):
        super().construct()
        self.write_column(1)
        self.sort_matrix()
        self.wait()
        self.highlight_column(1)
        self.wait(4)
